---
layout: post
title:  "En plein pic de pollution, Paris tourne le dos à la Solution Vélo"
date:   2016-12-07 08:00:00 +0100
author: Paris en Selle • Communiqué
categories: article
link: https://parisenselle.fr/pollution-paris-renonce-solution-velo/
image: assets/images/articles/2016-12-07-pes-pollution-paris-renonce-solution-velo.jpg
---

{% include image.html
            img=page.image
            link=page.image
            caption=page.title
            source-text=page.author
            source-link=page.link
%}

Lire cet [article]({{ page.link }}).

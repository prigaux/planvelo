---
layout: post
title:  "Paris en Selle lance l'Observatoire Plan Vélo"
date:   2017-02-16 08:50:00 +0100
author: Paris en Selle • Communiqué
categories: article
link: https://parisenselle.fr/observatoire-plan-velo-lancement/
image: assets/images/articles/2017-02-16-pes-observatoire-plan-velo-lancement.jpg
---

{% include image.html
            img=page.image
            link=page.image
            caption=page.title
            source-text=page.author
            source-link=page.link
%}

Lire cet [article]({{ page.link }}).
